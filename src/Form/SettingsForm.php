<?php

namespace Drupal\google_adwords_lite\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements the form controller.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'google_adwords_lite_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['google_adwords_lite.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('google_adwords_lite.settings');
    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General settings'),
      '#open' => TRUE,
    ];
    $form['general']['enable'] = [
      '#title' => $this->t('Enable Google AdWords Lite'),
      '#description' => $this->t('Add the AdWords code on pages.'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('enable'),
    ];
    $form['general']['admin-pages'] = [
      '#title' => $this->t('Enable AdWords on admin pages'),
      '#description' => $this->t('Also add the code to admin pages.'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('admin-pages'),
    ];
    $form['general']['admin-disable'] = [
      '#title' => $this->t('Disable for user 1'),
      '#description' => $this->t('Do not add the code for the main admin (uid 1).'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('admin-disable'),
    ];
    $form['general']['aw-tag'] = [
      '#title' => $this->t('AW-ID'),
      '#default_value' => $config->get('aw-tag'),
      '#maxlength' => 20,
      '#size' => 15,
      '#type' => 'textfield',
      '#field_prefix' => 'AW-',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Implements a form submit handler.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('google_adwords_lite.settings');
    $config
      ->set('enable', $form_state->getValue('enable'))
      ->set('admin-pages', $form_state->getValue('admin-pages'))
      ->set('admin-disable', $form_state->getValue('admin-disable'))
      ->set('aw-tag', $form_state->getValue('aw-tag'))
      ->save();
  }

}
