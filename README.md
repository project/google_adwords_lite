# Google AdWords Lite

Provides a very simple solution for adding the AW-123123123 code to a site.

Settings:
 * AW-ID field
 * Main on/off switch.
 * Option to display on admin pages (/admin et al).

## Credits / contact

Written and maintained by [Damien
McKenna](https://www.drupal.org/u/damienmckenna). The module was built as a
fork of the [GoogleTag Manager](https://www.drupal.org/project/gtm) module
written by [Anatoly Politsin](https://www.drupal.org/u/apolitsin), so thanks to
Anatoly for providing a good base to work from.

Ongoing development is sponsored by [Mediacurrent](https://www.mediacurrent.com/).

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the [project issue
queue](https://www.drupal.org/project/issues/google_adwords_lite).
